const gplay = require('google-play-scraper');
const fs = require('fs');

const scoreToLabel = (score) => {
    if (score === 1 || score === 2) {
        return '0';
    }

    if (score === 4 || score === 5) {
        return '1';
    }

    throw 'Cannot convert reviews with score of 3.';
}

const appId = process.argv[2];
if (!appId) {
    console.error('You must provide App ID to fetch.');
    return;
}

const fileName = `${appId}.tsv`;
const fileExists = fs.existsSync(fileName);
const stream = fs.createWriteStream(fileName, { 'flags': 'a' });
if (!fileExists) {
    // Write header row if file is getting created
    stream.write('Review');
    stream.write('\t');
    stream.write('Label');
    stream.write('\n');
}

(async () => {
    const reviews = await gplay.reviews({
        appId: appId,
        num: 2500,
    });

    if (reviews.length === 0) {
        console.error(`Reviews array is empty. Check if App ID '${appId}' is correct.`);
        return;
    }

    reviews
        .filter(review => review.text.trim().length > 10) // remove really short reviews
        .filter(review => review.score != 3) // remove neutral ratings
        .forEach(review => {
            stream.write(review.text);
            stream.write('\t');
            stream.write(scoreToLabel(review.score));
            stream.write('\n');
        });
})();
