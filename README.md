# Play Store Scraper

A small command line utility to collect mobile app reviews from the [Google Play Store](https://play.google.com/).

## Prerequisites

- [Nodejs](https://nodejs.org/en/)

## Usage

`node index <app_id>`, e.g. `node index com.whatsapp`

## Output

<app_id>.tsv (tab separated file) with a review text column, and a binary 1 (positive) or 0 (negative) sentiment column. Reviews with neutral rating (3-star) or extremely short reviews (<= 10 characters) are automatically discarded. Only the latest 2500 reviews are queried.
